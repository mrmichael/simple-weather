var express = require('express');
var serveStatic = require('serve-static');
var fs = require('fs');
var IP = process.env.IP || '127.0.0.1';
var PORT = process.env.PORT || 3000;
var app = express();
var indexPath = process.cwd() + '/index.html';

app.use('/assets', serveStatic('dist/'));
app.get('/', function(req, res) {
  fs.createReadStream(indexPath).pipe(res);
});
app.listen(PORT, IP, console.log.bind(this, 'SimpleWeather started %s:%s...', IP, PORT));
