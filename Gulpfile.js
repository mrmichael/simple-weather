var gulp = require('gulp');
var less = require('gulp-less');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var babelify = require('babelify');


gulp.task('build:app', function(){
  return browserify(['./src/app/index.jsx'], { extensions: ['.jsx'] })
    .transform(babelify)
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('dist'));
});


gulp.task('compile:less', function(){
  return gulp.src('src/less/style.less')
    .pipe(less())
    .pipe(gulp.dest('dist'));
});


gulp.task('copy:img', function(){
  return gulp.src('./src/img/**/*').pipe(gulp.dest('dist/img'));
});


gulp.task('copy:fonts', function(){
  return gulp.src('./src/fonts/**/*').pipe(gulp.dest('dist/fonts'));
});


gulp.task('build:dev', ['build:app', 'compile:less', 'copy:img', 'copy:fonts']);

gulp.task('watch', function(){
  gulp.watch(['./src/app/**/*', './src/less/**/*.less'], ['build:dev']);
});
