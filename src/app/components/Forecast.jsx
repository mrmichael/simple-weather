import React from 'react';
import {getIconClassNames} from '../common/icon-service';

class Forecast extends React.Component {
  render(){
    return (
      <div className="forecast">
        <i className={getIconClassNames(this.props.icon)}/>
        <span className="temp">
          <span className="high">{this.props.high}&deg;</span>
          <span className="low">{this.props.low}&deg;</span>
        </span>
        <span className="day">{this.props.day}</span>
      </div>
    );
  }
}

Forecast.propTypes = {
  icon: React.PropTypes.string,
  high: React.PropTypes.string,
  low: React.PropTypes.string,
  day: React.PropTypes.string
};

Forecast.defaultProps = {
  icon: '',
  high: '100',
  low: '0',
  day: ''
};

export default Forecast;
