import React from 'react';
import {getIconClassNames} from '../common/icon-service';
import classnames from 'classnames';

class CurrentTemp extends React.Component {
  render(){
    var classes = classnames({ span5: true, fade: true, in: !this.props.hidden });
    return (
      <section id="currentTemp" className={classes}>
      	<i className={getIconClassNames(this.props.icon)}/>
      	<h1>{this.props.condition.temp}<span className="degree">&deg;</span></h1>
      	<h2>{this.props.city}, {this.props.state}</h2>
      	<span className="date">{this.props.date}</span>
      </section>
    );
  }
}

CurrentTemp.propTypes = {
  condition: React.PropTypes.shape({
    temp: React.PropTypes.string
  }),
  icon: React.PropTypes.string,
  city: React.PropTypes.string,
  state: React.PropTypes.string,
  date: React.PropTypes.string
};

CurrentTemp.defaultProps = {
  condition: {},
  icon: '',
  city: '',
  state: '',
  date: ''
};

export default CurrentTemp;
