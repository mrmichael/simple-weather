import {dispatcher} from 'fluxify';
import request from 'superagent';
import {LOCATION_LOAD} from '../common/constants';

export default {
  /**
   * Get the location ID from yahoo weather service.
   * @param {string} query
   */
  load(query) {
    var URL = `//query.yahooapis.com/v1/public/yql?q=select * from geo.placefinder where text="${query}"&format=json`;
    request
      .get(URL)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (!err) {
          dispatcher.dispatch(LOCATION_LOAD, res.body.query.results.Result.woeid);
        }
      });
  }
};