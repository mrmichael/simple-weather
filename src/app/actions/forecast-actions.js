import {dispatcher} from 'fluxify';
import request from 'superagent';
import {FORECAST_LOAD} from '../common/constants';

export default {
  /**
   * Get the forecast data from yahoo weather service.
   * @param {string} woeid
   */
  load(woeid) {
    let URL = `//query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid="${woeid}"`;
    request
      .get(URL)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (!err) {
          dispatcher.dispatch(FORECAST_LOAD, res.body);
        }
      });
  }
};