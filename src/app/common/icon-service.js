// icon codes from yahoo! weather service
var icons = {
  "3": "lightning",
  "4": "lightning cloud",
  "5": "snow rain",
  "6": "sleet cloud",
  "7": "sleet cloud",
  "8": "sleet",
  "9": "drizzle",
  "10": "sleet cloud",
  "11": "showers",
  "12": "showers",
  "13": "flurries",
  "14": "snow cloud",
  "15": "flurries",
  "16": "snow",
  "17": "hail",
  "18": "sleet",
  "20": "fog",
  "21": "haze",
  "22": "haze",
  "23": "wind cloud",
  "24": "wind",
  "25": "thermometer low",
  "26": "cloud",
  "27": "cloud moon",
  "28": "cloud sun",
  "29": "cloud moon",
  "30": "cloud sun",
  "31": "moon",
  "32": "sun",
  "33": "moon",
  "34": "sun low",
  "35": "hail",
  "36": "thermometer high",
  "37": "rain cloud",
  "38": "rain cloud",
  "39": "rain cloud",
  "40": "showers",
  "41": "snow",
  "42": "snow",
  "43": "snow",
  "44": "cloud sun",
  "45": "rain",
  "46": "snow",
  "47": "lightning"
};

export function getIconByCode(code) {
  return icons[code];
};

export function getIconClassNames(icon) {
  return `climacon ${icon}`;
};
