export const WEATHER_GET = 'getWeather';
export const LOCATION_GET = 'getLocation';
export const FORECAST_LOAD = 'loadForecast';
export const LOCATION_LOAD = 'loadLocation';