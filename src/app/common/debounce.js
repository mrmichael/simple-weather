var timer;

// @module debounce
export default function debounce(fn, timeout, scope){
  clearTimeout(timer);
  timer = setTimeout(() => {
    fn.call(scope);
  }, timeout);
};
