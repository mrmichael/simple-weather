import React from 'react';
import Forecast from './components/Forecast';
import CurrentTemp from './components/CurrentTemp';
import LocationStore from './stores/LocationStore';
import ForecastStore from './stores/ForecastStore';
import forecastActions from './actions/forecast-actions';
import locationActions from './actions/location-actions';
import Flux from 'fluxify';
import {WEATHER_GET, LOCATION_GET} from './common/constants';
import debounce from './common/debounce';

class App extends React.Component {

  constructor() {
    this.state = {
      query: '',
      weather: {
        hidden: true,
        forecast: []
      }
    };
    
    this._handleQueryInput = this._handleQueryInput.bind(this);
  }

  componentDidMount(){
    LocationStore.on('change:woeid', (woeid) => {
      forecastActions.load(woeid);
    });

    ForecastStore.on('change:forecast', (forecast) => {
      this.setState({weather: forecast});
    });

    navigator.geolocation.getCurrentPosition(({coords}) => {
      let {latitude, longitude} = coords;
      locationActions.load(`${latitude}, ${longitude}`);
    });
  }

  _handleQueryInput(e){
    let query = e.target.value;
    this.setState({query});
    debounce(() => {
      locationActions.load(query);
    }, 500);
  }

  render(){
    return (
      <main>
        <input
          id="search-input"
          type="text"
          name="zip"
          placeholder="search..."
          value={this.state.query}
          onChange={this._handleQueryInput} />
        <CurrentTemp {...this.state.weather}/>
        <section id="extended-forecast">
          {this.state.weather.forecast.map(f => <Forecast {...f}/>)}
        </section>
      </main>
    );
  }
}

React.render(<App/>, document.getElementById('app'));
