import Flux from 'fluxify';
import {LOCATION_LOAD} from '../common/constants';

export default Flux.createStore({
  id: 'locationstore',
  initialState: {
    woeid: ''
  },
  actionCallbacks: {
    [LOCATION_LOAD](updater, woeid) {
      updater.set({woeid});
    }
  }
});
