import {getIconByCode} from '../common/icon-service';

class ForecastModel {
  constructor(data){
    var root = data.query.results.channel;
    this.city = root.location.city;
    this.state = root.location.region;
    this.wind = root.wind;
    this.atmosphere = root.atmosphere;
    this.astronomy = root.astronomy;
    this.title = root.item.title;
    this.link = root.item.link;
    this.condition = root.item.condition;
    this.forecast = root.item.forecast.map(item => {
      return {
        icon: getIconByCode(item.code),
        high: item.high,
        low: item.low,
        day: item.day
      };
    });
    this.image = root.image.url;
    this.date = root.item.pubDate;
    this.icon = getIconByCode(root.item.condition.code);
  }
}

export default ForecastModel;
