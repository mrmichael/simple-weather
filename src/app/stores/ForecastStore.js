import Flux from 'fluxify';
import ForecastModel from './ForecastModel';
import {FORECAST_LOAD} from '../common/constants';

export default Flux.createStore({
  id: 'forecaststore',
  initialState: {
    forecast: {}
  },
  actionCallbacks: {
    [FORECAST_LOAD](updater, data) {
      updater.set({forecast: new ForecastModel(data)});
    }
  }
});
